#!/usr/bin/env python

import rospy
from sensor_msgs.msg import Temperature, RelativeHumidity

def temperature_callback(data):
    rospy.loginfo(rospy.get_caller_id() + 'Temperature: %.1f', data.temperature)

def humidity_callback(data):
	rospy.loginfo(rospy.get_caller_id() + 'Humidity: %.1f', data.relative_humidity)

def listener():
    rospy.init_node('weather_logger', anonymous=True)

    rospy.Subscriber('/temperature', Temperature, temperature_callback)
    rospy.Subscriber('/relative_humidity', RelativeHumidity, humidity_callback)

    rospy.spin()

if __name__ == '__main__':
    listener()
